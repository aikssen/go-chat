## CHAT

This is a very simple example about how to use websockets in Go withing a chat application.
And also adds a simple tracing method to show how to use interfaces.

Build and run
```
$ go build -o chat
./chat # or ./chat -addr=":3000"  or   -addr="192.168.0.1:3000"

# or
$ gin run main.go
```

Test

```
$ go test github.com/aikssen/chat/...
```

Coverage
```
$ go test github.com/aikssen/chat/... -cover
```